//
public class Main{
	static ReplicaManager r = new ReplicaManager();
	public static void main(String[] args) {
		
		Thread t1 = (new Thread() {
			public void run(){
				while(true) {
					r.addReplica("Replica");
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { e.printStackTrace();}
				}
			}});
		t1.start();
		Thread t2 = (new Thread() {
			public void run(){
				while(true) {
					r.mostraReplicas();
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { e.printStackTrace();}
				}
			}});
		t2.start();
		

	}

}
