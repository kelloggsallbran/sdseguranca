	import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

import com.rmi.executable.ObjectRegistryInterface;
import com.rmi.executable.Place;
import com.rmi.executable.PlacesListInterface;
import com.rmi.executable.PlacesListManagerInterface;
import com.rmi.executable.RMIRegistry;
import com.rmi.executable.RMIReplicaManager;
import com.rmi.executable.RMISSLClientSocketFactory;
import com.rmi.executable.RMIServer;
import com.rmi.executable.ReplicasManagementInterface;



public class RMIClient{	

	public static void main(String args[]) throws RemoteException, InterruptedException{
		
		int portas[]= {2028,2029,2030,2031};
		
		
		Thread t = (new Thread() {
			public void run() {
				RMIRegistry.main(new String[0]);
				RMIReplicaManager.main(new String[0]);
				
				
				RMIServer.main(new String[]{Integer.toString(portas[0])});
				//RMIServer.main(new String[]{Integer.toString(portas[1])});
				//RMIServer.main(new String[]{Integer.toString(portas[2])});
				//RMIServer.main(new String[]{Integer.toString(portas[3])});
				
			}
		});
		t.start();
		
		Thread.sleep(5000);

		PlacesListManagerInterface manager;
		ReplicasManagementInterface manager2;
		PlacesListInterface server;
		ObjectRegistryInterface registry;


		try{
			/* ReplicaManager - Adiciona places a todos os placemanagers */
			manager  = (PlacesListManagerInterface) Naming.lookup("rmi://localhost:2024/replicamanager");

			Place p1 = new Place("3510", "Viseu");
			manager.addPlace(p1);

			Place p2 = new Place("1000", "Lisboa");
			manager.addPlace(p2);

			Place p3 = new Place("4000", "Tejo");
			manager.addPlace(p3);

			
			/* Registry - devolve um ReplicaManager */
			registry = (ObjectRegistryInterface) Naming.lookup("rmi://localhost:2023/registry");
			String addrRM = registry.resolve("1000");

			
			/* ReplicaManager - Devolve um PlaceManager / servidor */			
			manager = (PlacesListManagerInterface) Naming.lookup(addrRM);
			String addr = manager.getPlaceListAddress("1000");
			System.out.println(addrRM);


			 
			 
			Registry registry2 = LocateRegistry.getRegistry(
	                InetAddress.getLocalHost().getHostName(), Integer.parseInt(addr),
	                new RMISSLClientSocketFactory());

	            // "obj" is the identifier that we'll use to refer
	            // to the remote object that implements the "Hello"
	            // interface
			
			/* Servidor - Devolve um place */
			System.out.println("Invocar getPlace() no PlaceManager");
			server  = (PlacesListInterface) registry2.lookup(addr);
			
			String locality = server.getPlace("1000").getLocality();
			System.out.println("\tDevolveu " + locality);
			
			Thread.sleep(5000);
		
			
			
			/* DEBUG - Estas liga��es s�o s� para testes. � para retirar um servidor de funcionamento */ 
			
			/*manager2 = (ReplicasManagementInterface) Naming.lookup("rmi://localhost:2024/replicamanager");
			manager2.removeReplica("rmi://localhost:2029/placelist");
			
			Thread.sleep(5000);
			
			manager2 = (ReplicasManagementInterface) Naming.lookup("rmi://localhost:2024/replicamanager");
			manager2.removeReplica("rmi://localhost:2030/placelist");
			
			Thread.sleep(5000);
			
			manager2 = (ReplicasManagementInterface) Naming.lookup("rmi://localhost:2024/replicamanager");
			manager2.removeReplica("rmi://localhost:2031/placelist");*/
		
		
		} catch(Exception e) { }//cenas e coisas coisas e bananas
	}
}
