import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import com.rmi.executable.Place;

public interface PlacesListInterface extends Remote{
	public void addPlace(Place p) throws RemoteException;
	public ArrayList<Place> allPlaces() throws RemoteException;
	Place getPlace(String postalCode) throws RemoteException;
}