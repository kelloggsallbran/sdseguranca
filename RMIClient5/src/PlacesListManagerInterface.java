
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import com.rmi.executable.Place;

public interface PlacesListManagerInterface extends Remote{
	public void addPlace(Place p) throws RemoteException;
	public String getPlaceListAddress(String idObject) throws RemoteException;
}
