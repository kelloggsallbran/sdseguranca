package com.rmi.executable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface ReplicasManagementInterface extends Remote{
	public String addReplica(String replicaAddress) throws RemoteException;
	public void removeReplica(String replicaAddress) throws RemoteException;
}
