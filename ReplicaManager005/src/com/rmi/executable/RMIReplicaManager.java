package com.rmi.executable;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RMIReplicaManager {
	static Registry r=null;
	static PlacesListManagerInterface placeListHosts=null;
	public static void main(String[] args) {
		
		try{
			r = LocateRegistry.createRegistry(2024);
		}catch(RemoteException a){
			a.printStackTrace();
		}
		
		try{
			placeListHosts = new ReplicaManager();	
			r.rebind("replicamanager", placeListHosts );

			System.out.println("ReplicaManager server ready");
		}catch(Exception e) {
			System.out.println("ReplicaManager server main " + e.getMessage());}
	}

}
